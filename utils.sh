#/bin/sh

checkprogramsexists(){
	error=0
	programlist=("$@") # list of programs pass through
	for program in "${programlist[@]}"; do
		#echo "checking program - $program"
		if !  which $program >/dev/null  ;then
			echo "${program} not installed"
			error=1
		fi
	done

	if [[ $error -eq 1 ]]; then
		exit 1
	fi
}


android(){
	echo "Android on screen"
        echo "============================================"
	scrcpy
}

youtube(){
	echo "Youtube-dl"
        echo "============================================"
        echo "Insira o link: "
        read link
        youtube-dl -F $link
        echo "insira o codigo: "
        read code
        youtube-dl -f $code $link
}

simplehttp(){
	echo "python simple http server"
	echo "============================================"
	python -m SimpleHTTPServer
}

sysinfo(){
	echo "System Information"
	echo "============================================"
	echo ""
	neofetch
	echo "boot timer: "
	systemd-analyze
}

wifipasswords(){
	echo "Wifi Passwords"
	echo "============================================"
	dir="/etc/NetworkManager/system-connections"
	out=$(find $dir -type f )
	readarray -t out <<<"$out"
	for i in "${!out[@]}"; do
                printf "%s\t%s\n" "$i" "${out[$i]}"
        done

	read opt
	sudo cat "${out[$opt]}"
}

randomstring(){
	echo "Random String"
	echo "============================================"
	echo "Insira o numero de caracters: "
	read size
	cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w $size | head -n 1
}



#main program

programlist=("scrcpy" "youtube-dl" "neofetch" "systemd-analyze")
checkprogramsexists "${programlist[@]}"

if [  -z $1 ]; then

	clear

	echo "UTILS"
	echo "============================================"
	echo "1 - Android on screen"
	echo "2 - Youtube-dl"
	echo "3 - python simple http server"
	echo "4 - System Information"
	echo "5 - Wifi Passwords"
	echo "6 - Random String"

	read  option

else
	option=$1
fi

clear

case $option in
	1)
	android
	;;

	2)
	youtube
	;;

	3)
	simplehttp
	;;

	4)
	sysinfo
	;;

	5)
	wifipasswords
	;;

	6)
	randomstring
	;;

	*)
	clear
	exit 0
	;;
esac
