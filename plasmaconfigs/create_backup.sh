#!/bin/bash

echo "creating plama backup..."

tar -cf plasma-backup-`date +%Y-%m-%d--%H-%M` ~/.config/plasma*
