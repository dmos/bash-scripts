#!/bin/bash

get=$(brightnessctl -m)

#$(sudo brightnessctl s 10%)

#echo $get

IFS=',' read -ra split <<< "$get"

perc="${split[3]}"
perc=${perc::-1}
echo "${split[3]}" # get persentage

if [ "$1" == "up" ]
then
	if [ "$perc" -lt "100" ]
	then
        	echo "can up"
        	updateVal=$(( perc + 10))%
        	echo $updateVal
        	$(sudo brightnessctl set $updateVal)
	fi

fi

if [ "$1" == "down" ] 
then

if [ "$perc" -gt "0" ]
	then
        	echo "can down"
        	updateVal=$(( perc - 10))%
        	echo $updateVal
        	$(sudo brightnessctl set $updateVal)
	fi
fi
